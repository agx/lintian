#!/usr/bin/perl -w
# md5sums -- lintian collection script

# Copyright © 1998 Richard Braakman
# Copyright © 2019 Felix Lechner
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, you can find it on the World Wide
# Web at http://www.gnu.org/copyleft/gpl.html, or write to the Free
# Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston,
# MA 02110-1301, USA.

package Lintian::coll::md5sums;

no lib '.';

use strict;
use warnings;
use autodie;

use BerkeleyDB;
use IO::Async::Loop;
use IO::Async::Process;
use Path::Tiny;

use lib "$ENV{'LINTIAN_ROOT'}/lib";

use Lintian::Collect::Dispatcher qw(create_info);
use Lintian::Util qw(drop_relative_prefix safe_qx);

use constant EMPTY => q{};
use constant SPACE => q{ };
use constant COLON => q{:};
use constant NEWLINE => qq{\n};
use constant NULL => qq{\0};

sub collect {
    my ($pkg, $type, $dir) = @_;

    chdir("$dir/unpacked");

    my $loop = IO::Async::Loop->new;
    my $future = $loop->new_future;

    my @command= ('xargs', '--null', '--no-run-if-empty', 'md5sum', '--');
    my $errors;

    my $calculate = IO::Async::Process->new(
        command => [@command],
        stdin => { via => 'pipe_write' },
        stdout => { via => 'pipe_read' },
        stderr => { into => \$errors },
        on_finish => sub {
            my ($self, $exitcode) = @_;
            my $status = ($exitcode >> 8);

            if ($status) {
                my $message = "Command @command exited with status $status";
                $message .= COLON . NEWLINE . $errors
                  if length $errors;
                $future->fail($message);
                return;
            }

            $future->done('Done with @command');
            return;
        });

    my $dbpath = "$dir/md5sums.db";
    unlink $dbpath
      if -e $dbpath;

    my %h;
    tie %h, 'BerkeleyDB::Btree',
      -Filename => $dbpath,
      -Flags    => DB_CREATE
      or die "Cannot open file $dbpath: $! $BerkeleyDB::Error\n";

    $calculate->stdout->configure(
        on_read => sub {
            my ($stream, $buffref, $eof) = @_;

            local $\ = NULL;

            while($$buffref =~ s/^(\S*)\s+([^\n]*)\n//) {

                my $sum = $1;
                my $path = $2;

                unless (length $sum && length $path) {
                    $future->fail(
                        "syntax error in md5sum output: '$sum' '$path'");
                    next;
                }

                # remove relative prefix, if present
                $path = drop_relative_prefix($path);

                $h{$path} = $sum;
            }

            return 0;
        },
    );

    $loop->add($calculate);

    my @special;

    my $info = create_info($pkg, $type, $dir);
    foreach my $path ($info->sorted_index) {

        next
          unless $path->is_file;

        if (index($path->name, NEWLINE) >= 0) {
            push(@special, $path->name);

            next;
        }

        $calculate->stdin->write($path . NULL);
    }

    $calculate->stdin->close_when_empty;
    $future->get;

    for my $path (@special) {

        my $output = safe_qx('md5sum', '--', $path);
        my ($md5sum) = ($output =~ qr/^(\S+)/);

        next
          unless length $md5sum;

        $h{path} = $md5sum;
    }

    untie %h;

    return;
}

collect(@ARGV) unless caller;

1;

# Local Variables:
# indent-tabs-mode: nil
# cperl-indent-level: 4
# End:
# vim: syntax=perl sw=4 sts=4 sr et
